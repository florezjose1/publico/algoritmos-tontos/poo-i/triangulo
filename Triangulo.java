
/**
 * Un ejemplo que modela un Triangulo usando POO
 *
 * @author Ejercicio: (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Triangulo {

    //COMPLETE las tres propiedades
    float lado1;
    float lado2;
    float lado3;

    public Triangulo() {
    }

    public Triangulo(float lado1, float lado2, float lado3) {
        //COMPLETE
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.lado3 = lado3;
    }

    /*complete GET/SET*/
    /**
     * Método de acceso a la propiedad lado1
     *
     * @return el valor del lado 1 del triangulo
     */
    public float getLado1() {
        //COMPLETE
        return this.lado1;
    }//fin getLado1

    /**
     * *
     * Método para modificación del lado 1 del triangulo
     *
     * @param lado 1 del triángulo
     */
    public void setLado1(float lado1) {
        //COMPLETE
        this.lado1 = lado1;
    }//fin setLado1

    /**
     * Método de acceso a la propiedad lado2
     *
     * @return el valor del lado 2 del triangulo
     */
    public float getLado2() {
        //COMPLETE
        return this.lado2;
    }//fin getLado2

    /**
     * *
     * Método para modificación del lado 2 del triangulo
     *
     * @param lado 2 del triángulo
     */
    public void setLado2(float lado2) {
        //COMPLETE
        this.lado2 = lado2;
    }//fin setLado2

    /**
     * Método de acceso a la propiedad lado 3
     *
     * @return el valor del lado 3 del triangulo
     */
    public float getLado3() {
        //COMPLETE
        return this.lado3;
    }//fin getLado3

    /**
     * *
     * Método para modificación del lado 3 del triangulo
     *
     * @param lado 3 del triángulo
     */
    public void setLado3(float lado3) {
        //COMPLETE
        this.lado3 = lado3;
    }//fin setLado3

    public String getTipo() {
        String tipo = "Desconocido";

        if (this.lado1 == this.lado2 && this.lado2 == this.lado3) {
            tipo = "Equilatero";
        } else {
            if (this.lado1 == this.lado2 || this.lado2 == this.lado3 || this.lado1 == this.lado3) {
                tipo = "Isosceles";
            } else if (this.lado1 != this.lado2 && this.lado2 != this.lado3 && this.lado1 != this.lado3) {
                tipo = "Escaleno";
            }
            if (this.esRectangulo()) {
                tipo = tipo + " Rectángulo";
            }
        }//fin else

        return tipo;
    }//fin método getTipo

    public boolean esRectangulo() {
        float l1 = this.lado1 * this.lado1;
        float l2 = this.lado2 * this.lado2;
        float l3 = this.lado3 * this.lado3;
        if (this.lado1 == this.lado2 && l3 == l1 + l2  || 
                this.lado2 == this.lado3 && l1 == l2 + l3 || 
                this.lado1 == this.lado3 && l2 == l1 + l3) {
            return true;//complete
        } else if (l1 > l2 && l1 > l3 && l1 == l2 + l3 || 
                l2 > l1 && l2 > l3 && l2 == l1 + l3 ||
                l3 > l2 && l3 > l1 && l3 == l2 + l1) {
            return true;//complete
        }
        return false;//complete
    }//fin método esRectangulo

}//fin clase Triangulo

