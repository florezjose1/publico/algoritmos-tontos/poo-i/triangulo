### Triángulo

Determinar el tipo de triángulo dadas las medidas de sus lados.

1. El triángulo puede ser equilátero, todos sus lados iguales.
2. El triángulo puede ser isósceles, dos de sus lados iguales.
3. El triángulo puede ser escaleno, todos sus lados diferentes.
4. Si el triángulo es isósceles o escaleno puede ser rectángulo.

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
